/**
 * A class that holds information about a tree in NYC
 * 
 * @author Jack Booth
 * @version 4/23/2017
 */
public class Tree implements Comparable<Tree>
{
	private int tree_id;
//	diameter
	private int tree_dbh;
	private String status;
	private String health;
//	species name
	private String spc_common;
	private int zip;
	private String boroname;
	private double x_sp;
	private double y_sp;
	
	/**
	 * Constructs a new Tree object.
	 * @param id Unique identification number for each tree point
	 * @param diam Diameter of the tree, measured at approximately 54" / 137cm above the ground.
	 * @param status Indicates whether the tree is alive, standing dead, or a stump.
	 * @param health Indicates the user's perception of tree health. 
	 * @param spc Common name for species, e.g. "red maple"
	 * @param zip The tree's zip code.
	 * @param boro Name of borough in which tree point is located
	 * @param x X coordinate, in state plane. Units are feet
	 * @param y Y coordinate, in state plane. Units are feet
	 * @throws IllegalArgumentException Exception thrown when an illegal argument is inputed
	 */
	public Tree ( int id, int diam, String status, String health, String spc, int zip, String boro, double x, double y ) throws IllegalArgumentException
	{
		if(id < 0)
			throw new IllegalArgumentException();
		else
			tree_id = id;
		
		if(diam < 0)
			throw new IllegalArgumentException();
		else
			tree_dbh = diam;
		try{
			if(status.equalsIgnoreCase("Alive") || status.equalsIgnoreCase("Dead") || status.equalsIgnoreCase("Stump") || status == null || status.equals(""))
				this.status = status;
			else
				throw new IllegalArgumentException();
			
			if(health.equalsIgnoreCase("Good") || health.equalsIgnoreCase("Fair") || health.equalsIgnoreCase("Poor") || health == null || health.equals(""))
				this.health = health;
			else
				throw new IllegalArgumentException();
		}
		catch(NullPointerException e)
		{
			
		}
		
		if(spc == null || spc == "")
			throw new IllegalArgumentException();
		else
			spc_common = spc;
		
		if(zip < 0 || zip > 99999)
			throw new IllegalArgumentException();
		else
			this.zip = zip;
		if(boro!=null)
		{
			if(boro.equalsIgnoreCase("Manhattan") || boro.equalsIgnoreCase("Bronx") || boro.equalsIgnoreCase("Brooklyn") || boro.equalsIgnoreCase("Queens") || boro.equalsIgnoreCase("Staten Island"))
			{
				boroname = boro;
			}
			else
				throw new IllegalArgumentException();
		}
		else
			throw new IllegalArgumentException();
		x_sp = x;
		y_sp = y;
	}

	@Override
	public int compareTo(Tree tree)
	{
		if(this.getSpecies().toLowerCase().compareTo(tree.getSpecies().toLowerCase()) >0)
			return 1;
		else if(this.getSpecies().toLowerCase().compareTo(tree.getSpecies().toLowerCase())<0)
			return -1;
		else if(this.getSpecies().equalsIgnoreCase(tree.getSpecies()));
		{
			if(this.getTreeID()>tree.getTreeID())
				return 2;
			else if(this.getTreeID()<tree.getTreeID())
				return -2;
			else
				return 0;
		}
	}
	
	/**
	 * Tests to see if two trees are equal to each other in ID and species name
	 * @param tree The tree to compare to.
	 * @return true if two trees have the same id and species name, false if they have different ids and species names.
	 * @throws IllegalArgumentException Thrown when one variable (id or species name) is the same, but not the other
	 */
	@Override
	public boolean equals(Object tree) throws IllegalArgumentException
	{
		if(tree instanceof Tree)
		{
			if(((Tree) tree).tree_id == this.tree_id && ((Tree) tree).spc_common.equalsIgnoreCase(this.spc_common))
				return true;
			
			else
				return false;
		}
		else
			throw new IllegalArgumentException();
	}

	@Override
	public String toString()
	{
		return ("species: " + this.spc_common + "\ntree id: " + this.tree_id + "\n" + "diameter: " + tree_dbh + "\n" + "status: " + status + "\n" + "borough: " + boroname );
	}
	
	/**
	 * Checks to see if two trees have the same species name
	 * @param t tree to compare to
	 * @return true if species names are equal, false otherwise
	 */
	public boolean sameName(Tree t)
	{
		if(spc_common.equalsIgnoreCase(t.getSpecies()))
			return true;
		else
			return false;
	}
	
	/**
	 * Compares the species name of two trees
	 * @param t the tree to compare to
	 * @return 1 if the current species name is larger, -1 if it is smaller, and 0 if they are equal.
	 */
	public int compareName(Tree t)
	{
		if(this.getSpecies().toLowerCase().compareTo(t.getSpecies().toLowerCase()) >0)
			return 1;
		else if(this.getSpecies().toLowerCase().compareTo(t.getSpecies().toLowerCase())<0)
			return -1;
		else
			return 0;
	}
	
	/**
	 * Gets the ID of the tree.
	 * @return the tree's ID
	 */
	public int getTreeID()
	{
		return tree_id;
	}
	
	/**
	 * Gets the diameter of the tree.
	 * @return the tree's diameter
	 */
	public int getTreeDiam()
	{
		return tree_dbh;
	}
	
	/**
	 * Gets the status of the tree.
	 * @return the tree's status
	 */
	public String getStatus()
	{
		return status;
	}
	
	/**
	 * Gets the health of the tree.
	 * @return the tree's health
	 */
	public String getHealth()
	{
		return health;
	}
	
	/**
	 * Gets the species name of the tree.
	 * @return the tree's species name
	 */
	public String getSpecies()
	{
		return spc_common;
	}
	
	/**
	 * Gets the zip code of the tree.
	 * @return the tree's zip code
	 */
	public int getZip()
	{
		return zip;
	}
	
	/**
	 * Gets the borough name of the tree.
	 * @return the tree's borough name
	 */
	public String getBoroName()
	{
		return boroname;
	}
	
	/**
	 * Gets the x position in feet of the tree.
	 * @return the tree's x position in feet
	 */
	public double getX()
	{
		return x_sp;
	}
	
	/**
	 * Gets the y position in feet of the tree.
	 * @return the tree's y position in feet
	 */
	public double getY()
	{
		return y_sp;
	}
}
